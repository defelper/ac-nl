$(document).ready(function() {
    var dados;
    $.ajax({
        dataType: "html",
        type: 'Post',
        url: "app/view/listafurn.php",
        success: function(retorno) {
            var data = eval(retorno);
            var content = montarPagina(data);
            dados = content;
            $("#itemContainer").html(content);
            $("div.holder").jPages({
                containerID: "itemContainer"
            });

            $("div.sell").bind({
                click: function() {
                    window.location = 'comercio.php';
                }
            });
            $(".add").bind({
                click: function() {
                    var elemento = $(this).parent("li").children("div.namefurn").html();
                    $("#furnnamefurn").html(elemento);
                    $("#info").dialog("open");
                }
            });
        }
    });
    $("head").append("<script type=\"text/javascript\" src=\"js/menu.js\"><script>");

    $('#keyword').keyup(function(e) {
        //if (e.which === 13) {

        var encontrou = false;
        var termo = jQuery(this).val().toLowerCase();
        var log = "";
        jQuery('#itemContainer li').each(function() {
            var elementoCorrente = jQuery(this);
            var inputCorrente = elementoCorrente.children('input');
            var termoCorrente = inputCorrente.val().toLowerCase();

            if (termoCorrente.indexOf(termo) > -1)
                encontrou = true;
            if (!encontrou) {
                elementoCorrente.addClass("jp-hidden");
                elementoCorrente.hide();
                log += (termo + " - " + termoCorrente + " escondido\n");
            } else {
                elementoCorrente.removeClass("jp-hidden");
                elementoCorrente.show();
                log += (termo + " - " + termoCorrente + " mostrado\n");
            }
            encontrou = false;
        }); //finaliza EACH
        //} else {

        $("div.holder").jPages({
            containerID: "itemContainer"
        });
    }).keydown(function(e) {
        // prevent the enter key from submitting the form / closing the widget
        if (e.which === 13) {
            e.preventDefault();
        }
    });

    $("#info").dialog({
        autoOpen: false,
        draggable: false,
        resizable: false,
        my: "center",
        at: "center",
        of: window,
        height: 600,
        width: 350,
        modal: true,
        buttons: {
            "fechar": function() {
                $(this).dialog("close");
            }
        }
    });

});

function dump(obj) {
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }
    var pre = document.createElement('pre');
    pre.innerHTML = out;
    document.body.appendChild(pre);
}

function montarPagina(data) {
    var content = "";
    for (var i in data) {
        //montar aqui o conteudo
        content += '<li class="furns">';
        content += '<input type="hidden" name="name" id="namefurn" value="' + data[i].namefurn + '"/>';

        content += '<div class="namefurn">' + data[i].namefurn + '</div>';
        content += '<div class="add">+</div>';
        content += '<div class="standardprice"><span class="labelfurn">' +
                'Standard Price: </span><span class="valuefurn">' + data[i].standardprice + '</span></div>';
        content += '<div class="retailprice"><span class="labelfurn">' +
                'Retail Price: </span><span class="valuefurn">' + data[i].retailprice + '</span></div>';
        content += '<div class="avgprice"><span class="labelfurn">' +
                'Avg Price: </span><span class="valuefurn">' + data[i].avgprice + '</span></div>';
        content += '<div class="sell">$</div>';
        content += '</li>';
        //ta[i].idfurn + " - " + data[i].namefurn + "</li>";
    }
    return content;
}