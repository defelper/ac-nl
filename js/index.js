$(document).ready(function() {
    $("#formLogin").submit(function(e) {
        e.preventDefault();
        if (validarCamposLogin(this)) {
            var d = $(this).serialize();
            $.ajax({
                dataType: "html",
                type: "Post",
                url: "app/login.php",
                data: d,
                success: function(data) {
                    var retorno = $.parseJSON(data);
                    if (retorno.tipo === "sucesso") {
                        window.location = 'menu.php';
                    }
                    else {
                        alert(retorno.msg);
                    }
                }
            });
        }
    });
    $("#entrar").click(function() {
        $("#formLogin").submit();
    });
    $("#novo").click(function() {
        window.location = 'novo.php';
    });
});

function validarCamposLogin(frm) {
    var name = frm.name.value;
    var pass = frm.pass.value;
    if (name === "") {
        alert("Favor, preencha o campo usuario!");
        frm.name.focus();
        return false;
    } else if (pass === "") {
        alert("Favor, preencha o campo senha!");
        frm.pass.focus();
        return false;
    } else {
        return true;
    }

}

