$(document).ready(function() {
    $("#formulario").submit(function(e) {
        e.preventDefault();
        if (validacad(this)) {
            var d = $(this).serialize();
            alert("enviando");
            $.ajax({
                dataType: "html",
                type: "Post",
                url: "app/salvarcadastro.php",
                data: d,
                success: function(data) {
                    var retorno = $.parseJSON(data);
                    if (retorno.tipo === "erro") {
                        alert(retorno.msg);
                    } else if (retorno.tipo === "sucesso") {
                        alert(retorno.msg);
                        window.location = 'menu.php';
                    }
                }
            });
        }
    });
    
    $("#salvar").click(function(){
        $("#formulario").submit();
    });
    
    $("#voltar").click(function(){
        window.location="index.php";
    });
    
    $("#fccode").mask("9999-9999-9999");
});

function validacad(frm) {
    if ($.trim(frm.nome.value) === "") {
        alert("Favor, informe o nome!");
        frm.nome.focus();
        return false;
    } else {
        var fccode = frm.fccode.value;
        if (fccode.search(/\d{4}-\d{4}-\d{4}$/i) !== 0 || frm.fccode.value === "") {
            //if(frm.fccode.value===""){
            alert("Friend Code inv�lido, redigite!");
            frm.fccode.focus();
            return false;
        }
        else if ($.trim(frm.user.value) === "") {
            alert("Favor, informe o nome de usu�rio!");
            frm.user.focus();
            return false;
        } else if ($.trim(frm.pass.value) === "") {
            alert("Favor, informe o password!");
            frm.pass.focus();
            return false;
        } else {
            return true;
        }
    }
}