function initPagination(qtd) {
    $("#paginas").load("app/loaddata.php?action=get_rows&qtd=" + qtd);
    $.get("app/loaddata.php?action=row_count&qtd=" + qtd, function(data) {
        $("#page_count").val(Math.ceil(data / qtd)); // Store the number of pages in the hidden input #page_count
        displayLinks(1,qtd);
    });
}

// Create / refresh page links with first and last links based upon which page we are currently on (current_page)
function displayLinks(current_page,qtd) {
    var pages = $("#page_count").val();
    if (pages <= 5) {
        for (i = pages; i > 1; i--) {
            $("#paginas").after("a href='#' class='pagor'>" + i + "/a>");
        }

        $("#paginas").after("a href='#' class='pagor selected'>1/a>");

        $(".pagor").click(function() {
            var index = $(".pagor").index(this);
            $("#paginas").load("loaddata.php?action=get_rows&start=" + index);
            $(".pagor").removeClass("selected");
            $(this).addClass("selected");
        });
    } else {
        if (current_page < 5) {
// Draw the first 5 then have ... link to last
            var pagers = "<div id='paginator'>";
            for (i = 1; i <= 5; i++) {
                if (i === current_page) {
                    pagers += "<a href='#' class='pagor selected'>" + i + "</a>";
                } else {
                    pagers += "<a href='#' class='pagor'>" + i + "</a>";
                }
            }
            pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagor'>" + Number(pages) + "</a><div style='clear:both;'></div></div>";
            $("#paginator").remove();
            $("#paginas").after(pagers);
// Attach event listeners to our page links which will call updatePage function when clicked
            $(".pagor").click(function() {
                updatePage(this,qtd);
            });
        } else if (current_page > (Number(pages) - 4)) {
// Draw ... link to first then have the last 5
            var pagers = "<div id='paginator'><a href='#' class='pagor'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
            for (i = (Number(pages) - 4); i <= Number(pages); i++) {
                if (i === current_page) {
                    pagers += "<a href='#' class='pagor selected'>" + i + "</a>";
                } else {
                    pagers += "<a href='#' class='pagor'>" + i + "</a>";
                }
            }
            pagers += "<div style='clear:both;'></div></div>";
            $("#paginator").remove();
            $("#paginas").after(pagers);
// Attach event listeners to our page links which will call updatePage function when clicked
            $(".pagor").click(function() {
                updatePage(this, qtd);
            });
        } else {
// Draw the number 1 element, then draw ... 2 before and two after and ... link to last
            var pagers = "<div id='paginator'><a href='#' class='pagor'>1</a><div style='float:left;padding-left:6px;padding-right:6px;'>...</div>";
            for (i = (Number(current_page) - 2); i <= (Number(current_page) + 2); i++) {
                if (i === current_page) {
                    pagers += "<a href='#' class='pagor selected'>" + i + "</a>";
                } else {
                    pagers += "<a href='#' class='pagor'>" + i + "</a>";
                }
            }
            pagers += "<div style='float:left;padding-left:6px;padding-right:6px;'>...</div><a href='#' class='pagor'>" + pages + "</a><div style='clear:both;'></div></div>";
            $("#paginator").remove();
            $("#paginas").after(pagers);
// Attach event listeners to our page links which will call updatePage function when clicked
            $(".pagor").click(function() {
                updatePage(this,qtd);
            });
        }
    }
}

function updatePage(clicked_element, qtd) {
// Get the current_page based upon the text of the selected element
    var selected = $(clicked_element).text();
// First update content
    $("#paginas").load("app/loaddata.php?action=get_rows&qtd=" + qtd + "&start=" + (selected - 1));
// Then update links
    displayLinks(selected, qtd);
}

function loadPage(){
    
}