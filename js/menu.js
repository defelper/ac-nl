$(document).ready(function(){
    
    $('#menu').click(function(){
        $(this).hide('slow');
        $(this).delay(1000);
        $(this).show('slow');
    });
    
    $('#logout').click(function(){
        window.location = 'app/logout.php';
    });
});