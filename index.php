<?php
if (session_id() == "") {
	session_start("ses");
	if(isset($_SESSION["user"])){
		header("Location: menu.php");
	}
} 
?>
<!DOCTYPE html>
<html>
	<head>
		<title>AC:NL</title>
		<?php include 'app/charset.php';?>
		<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
		<script type="text/javascript" src="js/index.js"></script>
		<link rel="stylesheet" type="text/css" href="css/index.css" media="screen" />
	</head>
	<body>
		<div id="banner">
			IMAGEM DO BANNER
		</div>
		<div id="login">
			<h1>Login</h1>
			<form method="post" id="formLogin">
				<div class="user-box">
					<div>Usu&aacute;rio:</div>
						<input type="text" name="name" id="name" placeholder="username" />
				</div>
				<div class="pass-box">
					<div>Senha:</div>
						<input type="password" name="pass" id="pass" placeholder="password" />
				</div>
				<input type="button" value="Acessar" name="acao" id="entrar" class="button" />
				<input type="button" value="Novo Cadastro" name="cadastro" id="novo" class="button" />
			</form>
			<div id="result"></div>
		</div>
	</body>
</html>
