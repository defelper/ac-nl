<?php
if (session_id() == "") {
    session_start("ses");
    if (!isset($_SESSION["user"])) {
        header("Location: index.php");
    }
    if (!isset($_SESSION["qtd"])) {
        $_SESSION["qtd"] = 10;
    }
    if (!isset($_SESSION["inicio"])) {
        $_SESSION["inicio"] = 0;
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include 'app/charset.php'; ?>
        <link rel="stylesheet" href="css/jquery-ui-1.10.3.custom.css"/>
        <script type="text/javascript" src="js/jquery-1.9.1.js"></script>        
        <script type="text/javascript" src="js/jquery-ui-1.10.3.custom.min.js"></script>

<!--<script type="text/javascript" src="js/pagination.js"></script>-->
        <script type="text/javascript" src="js/catalogo.js"></script>
        <link type="text/css" rel="stylesheet" href="css/layout.css" media="screen">         
        <link type="text/css" rel="stylesheet" href="css/catalogo.css" media="screen"> 


        <link rel="stylesheet" href="css/jPages.css">
        <script src="js/jPages.js"></script>
        <title>AC:NL - Catalogo</title>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <?php
                include 'app/view/criar_header.php';
                ?>
            </div>
            <div id="menu">
                <?php
                include 'app/view/criar_menu.php';
                ?>
            </div>
            <div id="contents">
                <div id="search">
                    <label for="keyword">Pesquisar:</label>
                    <input type="text" id="keyword" placeholder="Digite o nome do item para buscar" size="30">
                </div>
                <!-- Future navigation panel -->
                <div class="holder"></div>
                <!-- Item container (doesn't need to be an UL) -->
                <div id="furnContainer">
                    <ul id="itemContainer" style="list-style-type: none">
                        <!-- Items -->
                    </ul>
                </div>
            </div>
            <div id="info" class="hidden">
                <div class="namefurn" id="furnnamefurn"></div>
                <div class="standardprice"><span id="furnstandardprice" class="labelfurn">Standard Price: </span>
                    <span class="valuefurn"></span></div>
                <div class="retailprice"><span id="furnretailprice" class="labelfurn">Retail Price: </span>
                    <span class="valuefurn"></span></div>
                <div class="avgprice"><span id="furnavgprice" class="labelfurn"></span>
                    <span class="valuefurn"></span></div>

            </div>
        </div>
    </body>
</html>
