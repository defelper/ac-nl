<?php

class Furniture {
    public $idfurn;
    public $namefurn;
    public $standardprice;
    public $color1;
    public $color2;
    public $theme;
    public $style;
    public $source;
    public $avgprice;
    public $pos;
    public $neg;
    public $retailprice;
}

?>
