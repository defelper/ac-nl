<?php

class Connection extends PDO {

    private static $engine;
    private static $host;
    private static $database;
    private static $user;
    private static $pass;

    public function __construct() {
        self::$engine = 'mysql';
        self::$host = 'localhost';
        self::$database = 'acnlbase';
        //self::$host = 'mysql.qlix.com.br';
        //self::$database = 'furnleafanimal';
        self::$user = 'root';
        self::$pass = '';
        $dns = self::$engine . ':dbname=' . self::$database . ";host=" . self::$host;
        parent::__construct($dns, self::$user, self::$pass);
    }

}

?>
