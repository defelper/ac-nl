<?php

include_once 'connection.php';
include_once 'model/user.php';
include_once 'control/userdao.php';

$user = new User();
$user->name = $_POST["nome"];
$user->fccode = $_POST["fccode"];
$user->username = $_POST["user"];
$user->pass = md5($_POST["pass"]);

$bd = new Connection();
$userdao = new UserDAO($bd);
$jsonreturn = "{";

if ($userdao->consultUserByParam($user, "fccode")) {
    $jsonreturn = $jsonreturn . '"tipo": "erro",';
    $jsonreturn = $jsonreturn . '"msg": "Friend code j� existe"}';
} else if ($userdao->consultUserByParam($user, "username")) {
    $jsonreturn = $jsonreturn . '"tipo": "erro",';
    $jsonreturn = $jsonreturn . '"msg": "Usu�rio j� existe"}';
} else if ($userdao->addUser($user)) {
    $jsonreturn = $jsonreturn . '"tipo": "sucesso",';
    $jsonreturn = $jsonreturn . '"msg": "Usu�rio salvo"}';
} else {
    $jsonreturn = $jsonreturn . '"tipo": "erro",';
    $jsonreturn = $jsonreturn . '"msg": "Usu�rio j� existe"}';
}
echo $jsonreturn;
?>
