<?php

include_once './connection.php';
include_once './model/furniture.php';
include_once './control/furnituredao.php';

if (isset($_REQUEST['action'])) {
    $action = $_REQUEST['action'];

    switch ($action) { // We will determine which function to call based on an action paramater passed in through the url
        case 'get_rows':
            displayPage();
            break;
        case 'row_count':
            getRowCount(); // Return the number of rows in the database
            break;
        default;
            break;
    }
    exit;
} else {
    return false;
}

function getRowCount() {
    $db = new Connection();
    $furndao = new FurnitureDAO($db);
    $result = $furndao->getAll();
    $count = count($result);
    echo $count;
}

// Retrieves raw data from the database then formats it and then returns the formatted rows as a string
function displayPage() {
    $qtd = isset($_REQUEST['qtd'])? $_REQUEST['qtd']: 10;
    $qtd = (int) $qtd;
    $start_row = isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
    $start_row =  $qtd * (int) $start_row;

    $furnitures = getNames($start_row);

    $formatted_names = "<div id='formatted_furnitures'>" . formatData($furnitures) . "</div>";

    echo $formatted_names;
}

// Retrieves rows from the names table
function getNames($start_row = 0) {
    $qtd = isset($_REQUEST['qtd'])? $_REQUEST['qtd']:10;
    $qtd = (int) $qtd;
    $db = new Connection();
    $furndao = new FurnitureDAO($db);
    $result = $furndao->getList($qtd, $start_row);
    return $result;
}

// Returns the rows as a formatted string
function formatData($data) {
    $formatted = '';
    foreach ($data as $dat) {
        $formatted .= '<div id="furn">' . $dat->namefurn . ' ' . $dat->standardprice . '</div>';
    }
    return $formatted;
}

?>