<?php
session_start("ses");
include_once '../connection.php';
include_once '../model/user.php';
include_once '../model/furniture.php';
include_once '../model/furnitureinventario.php';
include_once '../control/inventariodao.php';

$user = unserialize($_SESSION["user"]);

$inventariodao = new InventarioDAO(new Connection());
$listinventario = $inventariodao->getByUser($user);
$tipo = (isset($_REQUEST["tipo"])) ? $_REQUEST["tipo"] : "all";

//passando a pagina como retorno, mas jah deixei o json de todos prontos
$strjson = json_encode($listinventario);
echo $strjson;

?>