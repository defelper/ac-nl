<?php

session_start("ses");
include_once '../connection.php';
include_once '../model/furniture.php';
include_once '../control/furnituredao.php';

$tipo = (isset($_REQUEST["tipo"])) ? $_REQUEST["tipo"] : "all";

switch ($tipo) { // We will determine which function to call based on an action paramater passed in through the url
    case 'query':
        $query = (isset($_REQUEST["query"])) ? $_REQUEST["query"] : "";
        $furnlist = getQuery($query);
        break;
    case 'all':
        $furnlist = getAll(); // Return the number of rows in the database
        break;
    default;
        break;
}
//passando a pagina como retorno, mas jah deixei o json de todos prontos
$strjson = json_encode($furnlist);
echo $strjson;

function getAll() {
    $bd = new Connection();
    $furndao = new FurnitureDAO($bd);
    return $furndao->getAll();
}

function getQuery($query) {
    $bd = new Connection();
    $furndao = new FurnitureDAO($bd);
    return $furndao->getByName($query);
}
?>