<?php

class InventarioDAO{
    
    private $bd;
    
    public function __construct(Connection $bd) {
        $this->bd = $bd;
    }
    
    public function getByUser(User $user){
        $st = $this->bd->prepare("select furnitures.*,
            inventory.qtd,inventory.price from users,furnitures,inventory 
            where users.iduser=inventory.iduser and 
            inventory.idfurn=furnitures.idfurn and users.iduser= :iduser");
        $st->bindParam(":iduser",$user->iduser,PDO::PARAM_INT);
        $st->execute();
        if ($st->rowCount() > 0) {
            return $this->processResults($st);
        }
        return false;
    }
    
    public function getListUserByIDFurn($idfurn){
        $st = $this->bd->prepare("select users.* from users,inventory where 
            users.iduser=inventory.iduser and inventory.idfurn=:idfurn");
        $st->bindParam(":idfurn",$idfurn,PDO::PARAM_INT);
        $st->execute();
        if ($st->rowCount() > 0) {
            return $this->processResults($st);
        }
        return false;
    }
    
    public function insertInventario(FurnitureInventario $furn){
        $st=  $this->bd->prepare("insert into inventory(iduser,idfurn,price,qtd)
            values (:iduser,:idfurn,:price,:qtd)");
        $st->bindParam(":idfurn", $furn->iduser, PDO::PARAM_INT);
        $st->bindParam(":iduser", $furn->idfurn, PDO::PARAM_INT);
        $st->bindParam(":price", $furn->price, PDO::PARAM_INT);
        $st->bindParam(":qtd", $furn->qtd, PDO::PARAM_INT);
        $st->execute();
        if($st){
            return true;
        }
        return false;
    }
    
    public function deleteInventario(FurnitureInventario $furn){
        $st=  $this->bd->prepare("delete from inventory where iduser=:iduser and idfurn=:idfurn");
        $st->bindParam(":idfurn", $furn->iduser, PDO::PARAM_INT);
        $st->bindParam(":iduser", $furn->idfurn, PDO::PARAM_INT);
        $st->execute();
        if($st){
            return true;
        }
        return false;
    }
    
    public function updateInventario(FurnitureInventario $furn){
        $st=  $this->bd->prepare("update inventory set price=:price,qtd=:qtd
            where idfurn=:idfurn and iduser=:iduser");
        $st->bindParam(":idfurn", $furn->iduser, PDO::PARAM_INT);
        $st->bindParam(":iduser", $furn->idfurn, PDO::PARAM_INT);
        $st->bindParam(":price", $furn->price, PDO::PARAM_INT);
        $st->bindParam(":qtd", $furn->qtd, PDO::PARAM_INT);
        $st->execute();
        if($st){
            return true;
        }
        return false;
    }
    
    private function processResults($statement) {
        $results = array();

        if ($statement) {
            while ($row = $statement->fetchObject("FurnitureInventario")) {
                $results[] = $row;
            }
        }

        return $results;
    }
}

?>
