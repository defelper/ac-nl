<?php

class UserDAO {

    private $bd;

    public function __construct(Connection $bd) {
        $this->bd = $bd;
    }

    public function addUser($user) {
        $st = $this->bd->prepare(
                "insert into users(username,pass,name,fccode) values
                (:username , :pass , :name, :fccode)"
        );
        $st->execute(array(
            ':username' => $user->username,
            ':pass' => $user->pass,
            ':name' => $user->name,
            ':fccode' => $user->fccode
        ));
        if ($st) {
            return true;
        } return false;
    }

    public function deleteUser($user) {
        $iduser = $user->idUser;
        $st = $this->bd->prepare("delete from users where iduser= :iduser");
        $st->execute(array(':iduser' => $iduser));
    }

    public function consultUserByParam($user, $param) {
        $st = $this->bd->prepare(
                "select * from users where $param = :value"
        );
        $st->bindParam(":value", $user->$param);
        $st->execute();
        if ($st->rowCount() > 0) {
            return true;
        } return false;
    }

}

?>
