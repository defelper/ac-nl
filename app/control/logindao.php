<?php

class LoginDAO {

    private $bd;

    public function __construct(Connection $bd) {
        $this->bd = $bd;
    }

    public function validateLogin(User &$user) {


        $st = $this->bd->prepare("Select * from users where username=:username");


        $st->execute(array(":username" => $user->username));
        if($st->rowCount() > 0){
            $user = $st->fetchObject("User");
            return true;
        }
        
        return false;

        
    }

}

