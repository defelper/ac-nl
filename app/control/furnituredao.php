<?php

class FurnitureDAO {

    private $bd;

    public function __construct(Connection $bd) {
        $this->bd = $bd;
    }

    public function getList($qtd, $inicio) {

        $st = $this->bd->prepare("Select * from furnitures ORDER BY idfurn limit :qtd offset :inicio");
        //$st = $this->bd->prepare("Select * from furnitures ORDER BY rand() limit :qtd");
        $st->bindParam(":qtd", $qtd, PDO::PARAM_INT);
        $st->bindParam(":inicio", $inicio, PDO::PARAM_INT);
        $st->execute();
        if ($st->rowCount() > 0) {
            return $this->processResults($st);
        }
        return false;
    }

    public function getAll() {
        $st = $this->bd->prepare("Select * from furnitures ORDER BY idfurn");
        $st->execute();
        if ($st->rowCount() > 0) {
            $list = $this->processResults($st);
            return $list;
        }
        return false;
    }
    
    public function getByID($idfurn){
        $st=  $this->bd->prepare("Select * from furnitures where idfurn=:idfurn");
        $st->bindParam(":idfurn", $idfurn);
        $st->execute();
        if($st->rowCount()>0){
            return $st->fetchObject("Furniture");
        }
        return false;
    }
    
     public function getByName($namefurn){
        $namefurn = "%".$namefurn."%";
        $st=$this->bd->prepare("Select * from furnitures where namefurn like :namefurn");
        $st->bindParam(":namefurn", $namefurn);
        $st->execute();
        if($st->rowCount()>0){
            return $this->processResults($st);
        }
        return false;
    }

    private function processResults($statement) {
        $results = array();

        if ($statement) {
            while ($row = $statement->fetchObject("Furniture")) {
                $results[] = $row;
            }
        }

        return $results;
    }

    public function estanoarray($id, $furnlist) {
        $f = new FurnitureInventario();
        foreach ($furnlist as $f) {
            if ($f->idfurn === $id) {
                return true;
            }
        }
        return false;
    }

}

?>