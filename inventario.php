<?php
if (session_id() == "") {
    session_start("ses");
    if (!isset($_SESSION["user"])) {
        header("Location: index.php");
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include 'app/charset.php'; ?>
        <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="js/inventario.js"></script>
        <link type="text/css" rel="stylesheet" href="css/layout.css" media="screen"> 
        <link rel="stylesheet" href="css/inventario.css">
        <link rel="stylesheet" href="css/jPages.css">
        <script src="js/jPages.js"></script>
        <title>AC:NL - Inventario</title>
    </head>
    <body><div id="container">
            <div id="header">
                <?php
                include 'app/view/criar_header.php';
                ?>
            </div>
            <div id="menu">
                <?php
                include 'app/view/criar_menu.php';
                ?>
            </div>
            <div id="contents">
                <div id="search">
                    <label for="keyword">Pesquisar:</label>
                    <input type="text" id="keyword" placeholder="Digite o nome do item para buscar" size="30">
                </div>
                <!-- Future navigation panel -->
                <div class="holder"></div>
                <!-- Item container (doesn't need to be an UL) -->
                <div id="furnContainer">
                    <ul id="itemContainer" style="list-style-type: none">
                        <!-- Items -->
                    </ul>
                </div>
            </div>

    </body>
</html>

