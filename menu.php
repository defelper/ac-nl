<?php
if (session_id() == "") {
    session_start("ses");
    if (!isset($_SESSION["user"])) {
        header("Location: index.php");
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include 'app/charset.php'; ?>
        <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="js/menu.js"></script>
        <link type="text/css" rel="stylesheet" href="css/layout.css" media="screen"> 
        <link type="text/css" rel="stylesheet" href="css/style.css" media="screen"> 
        <title>AC:NL Menu</title>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <?php
                include 'app/view/criar_header.php';
                ?>
            </div>
            <div id="menu">
                <?php
                include 'app/view/criar_menu.php';
                ?>
            </div>
            <div id="contents">
                <div id="aleatorio">
                    conteudo
                </div>
            </div>
        </div>
    </body>
</html>
