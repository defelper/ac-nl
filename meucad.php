<?php
if (session_id() == "") {
    session_start("ses");
    if (!isset($_SESSION["user"])) {
        header("Location: index.php");
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <?php include 'app/charset.php'; ?>
        <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="js/menu.js"></script>
        <link type="text/css" rel="stylesheet" href="css/layout.css" media="screen"> 
        <link type="text/css" rel="stylesheet" href="css/style.css" media="screen"> 
        <title>AC:NL Menu</title>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <?php
                include 'app/view/criar_header.php';
                ?>
            </div>
            <div id="menu">
                <?php
                include 'app/view/criar_menu.php';
                ?>
            </div>
            <div id="contents">
                <div>
                    <?php $user = unserialize($_SESSION["user"]); ?>
                    <form id="formulario" method="post">
                        <p align="left">
                            <label>Nome:
                                <input type="text" name="nome" id="nome" value="<?php echo $user->name ?>"/>
                            </label> 
                        </p>
                        <p align="left">
                            <label>Friend Code:
                                <input type="text" name="fccode" id="fccode" value="<?php echo $user->fccode ?>"/>
                            </label>
                        </p>
                        <p align="left">
                            <label>Nome de Usu�rio:
                                <input type="text" name="user" id="user" value="<?php echo $user->username ?>"/>
                            </label>
                        </p>
                        <p align="left">
                            <label>Senha:
                                <input type="password" name="pass" id="pass" value="<?php echo $user->pass ?>"/>
                            </label>
                        </p>
                        <p align="left">&nbsp;</p>
                        <p align="left">
                            <input type="button" value="Salvar" id="salvar"/>
                            <input type="button" value="Voltar" id="voltar"/>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>

